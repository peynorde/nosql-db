require 'time'

import 'org.apache.hadoop.hbase.client.HTable'
import 'org.apache.hadoop.hbase.client.Put'
import 'javax.xml.stream.XMLStreamConstants'

def jbytes(*args)
  args.map { |arg| arg.to_s.to_java_bytes }
end

factory = javax.xml.stream.XMLInputFactory.newInstance
reader = factory.createXMLStreamReader(java.lang.System.in)

document = nil
buffer = nil
count = 0

table = HTable.new(@hbase.configuration, 'foods')
table.setAutoFlush(false)

while reader.has_next
  type = reader.next

  if type == XMLStreamConstants::START_ELEMENT

    case reader.local_name
    when 'Food_Display_Row' then document = {}
    when /Food_Code|Display_Name|timestamp/ then buffer = []
    end

    elsif type == XMLStreamConstants::CHARACTERS

      buffer << reader.text unless buffer.nil?

    elsif type == XMLStreamConstants::END_ELEMENT

    case reader.local_name
    when /Food_Code|Display_Name|timestamp/
      document[reader.local_name] = buffer.join
    when 'Food_Code'
      key = document['Food_Code'].to_java_bytes
      ts = (Time.parse document['timestamp']).to_i

      p = Put.new(key, ts)
      p.add(*jbytes("Display_Name", "", document['Display_Name']))
      p.add(*jbytes("Food_Code", "Food_Code", document['Food_Code']))
      table.put(p)

      count += 1
      table.flushCommits() if count % 10 == 0
      if count % 500 == 0
        puts "#{count} records inserted (#{document['Food_Display_Row']})"
      end
    end
    end
    end

    table.flushCommits()
    exit

    